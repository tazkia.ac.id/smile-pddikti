package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

@Data
public class InsertMahasiswaResponse {
    private String error_code;
    private String error_desc;
    private IdMahasiswa data;
}
