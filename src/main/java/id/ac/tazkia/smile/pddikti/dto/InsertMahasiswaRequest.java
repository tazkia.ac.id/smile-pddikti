package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

@Data
public class InsertMahasiswaRequest {
   private String act = "InsertBiodataMahasiswa";
   private String token;
   private  BiodataMahasiswa record;
}
