package id.ac.tazkia.smile.pddikti.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class BiodataMahasiswa {

    private String nama_mahasiswa;
    private String jenis_kelamin;
    private String tempat_lahir;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal_lahir;
    private String id_agama;
    private String telepon;
    private String handphone;
    private String email;
    private String nik;
    private String kewarganegaraan;
    private String kelurahan;
    private Number penerima_kps;
    private String nama_ibu_kandung;
    private String nama_ayah;
    private Number id_kebutuhan_khusus_mahasiswa;
    private Number id_kebutuhan_khusus_ayah;
    private Number id_kebutuhan_khusus_ibu;

}
