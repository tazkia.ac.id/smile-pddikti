package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

@Data
public class TokenData {
    private String token;
}
