package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

@Data
public class GetTokenRequest {
    private String username;
    private String password;
    private String act = "GetToken";
}
