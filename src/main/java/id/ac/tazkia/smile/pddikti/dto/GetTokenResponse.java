package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class GetTokenResponse {
    private String error_code;
    private String error_desc;
    private TokenData data;
}
