package id.ac.tazkia.smile.pddikti.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.smile.pddikti.dao.MahasiswaDao;
import id.ac.tazkia.smile.pddikti.dto.*;
import id.ac.tazkia.smile.pddikti.entity.JenisKelamin;
import id.ac.tazkia.smile.pddikti.entity.Mahasiswa;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Locale;

@Service
public class PdDiktiService {
    @Value("${feeder.url}") private String feederUrl;
    @Value("${feeder.username}") private String feederUsername;
    @Value("${feeder.password}") private String feederPassword;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private MahasiswaDao mahasiswaDao;

    private String token;

    private WebClient webClient;

    @PostConstruct
    public void inisialisasiWebClient() {
        webClient = WebClient.builder()
                .baseUrl(feederUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public String getToken() {
        GetTokenRequest dto = new GetTokenRequest();
        dto.setUsername(feederUsername);
        dto.setPassword(feederPassword);

        try {
            String jsonRequest = objectMapper.writeValueAsString(dto);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        GetTokenResponse response = webClient.post()
                .body(Mono.just(dto), GetTokenRequest.class)
                .retrieve().bodyToMono(GetTokenResponse.class)
                .block();


        if (response.getData() == null) {
            return null;
        }

        return response.getData().getToken();
    }

    public BiodataMahasiswa biodataMahasiswa() {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim("172101053");

        BiodataMahasiswa bio = new BiodataMahasiswa();
        bio.setNama_mahasiswa(WordUtils.capitalize(mahasiswa.getNama()));
        if (mahasiswa.getJenisKelamin() == JenisKelamin.WANITA) {
            bio.setJenis_kelamin("P");
        } else if (mahasiswa.getJenisKelamin() == JenisKelamin.PRIA) {
            bio.setJenis_kelamin("L");
        }
        bio.setTempat_lahir(mahasiswa.getTempatLahir().toUpperCase(Locale.ROOT));
        bio.setTanggal_lahir(mahasiswa.getTanggalLahir());
        bio.setId_agama(mahasiswa.getIdAgama().getIdAgama());
        bio.setTelepon(mahasiswa.getTeleponRumah());
        bio.setHandphone(mahasiswa.getTeleponSeluler());
        bio.setEmail(mahasiswa.getEmailPribadi());
        bio.setNik(mahasiswa.getNik());
        bio.setKewarganegaraan("ID");
        bio.setKelurahan(mahasiswa.getIdKelurahan().toUpperCase(Locale.ROOT));
        System.out.println("Kelurahannya nihh cok :" + mahasiswa.getIdKelurahan().toUpperCase(Locale.ROOT));
        if (mahasiswa.getKps().equals("Tidak")) {
            bio.setPenerima_kps(0);
        } else if (mahasiswa.getKps().equals("Iya")) {
            bio.setPenerima_kps(1);
        }
        bio.setNama_ibu_kandung(mahasiswa.getIbu().getNamaIbuKandung());
        bio.setNama_ayah(mahasiswa.getAyah().getNamaAyah());
        bio.setId_kebutuhan_khusus_mahasiswa(0);
        bio.setId_kebutuhan_khusus_ayah(0);
        bio.setId_kebutuhan_khusus_ibu(0);

        return bio;
    }

    public String insertMahasiswa(){
        return insertMahasiswa(biodataMahasiswa());
    }

    public String insertMahasiswa(BiodataMahasiswa biodata){
        InsertMahasiswaRequest dto = new InsertMahasiswaRequest();
        dto.setToken(getToken());
        dto.setRecord(biodata);


        try {
            String jsonRequest = objectMapper.writeValueAsString(dto);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        InsertMahasiswaResponse response = webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(dto), InsertMahasiswaRequest.class)
                .retrieve().bodyToMono(InsertMahasiswaResponse.class)
                .block();

        if (response == null) {
            return null;
        }

        return  response.getData().getId_mahasiswa();
    }

}
