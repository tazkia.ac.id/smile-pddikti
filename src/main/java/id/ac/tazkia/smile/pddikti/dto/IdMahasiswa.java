package id.ac.tazkia.smile.pddikti.dto;

import lombok.Data;

@Data
public class IdMahasiswa {
    private String id_mahasiswa;
}
