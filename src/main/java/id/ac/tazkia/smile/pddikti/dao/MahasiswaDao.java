package id.ac.tazkia.smile.pddikti.dao;


import id.ac.tazkia.smile.pddikti.entity.Mahasiswa;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MahasiswaDao extends PagingAndSortingRepository<Mahasiswa,String> {
    Mahasiswa findByNim(String nim);
}
