package id.ac.tazkia.smile.pddikti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmilePddiktiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmilePddiktiApplication.class, args);
	}

}
