package id.ac.tazkia.smile.pddikti;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.smile.pddikti.dto.BiodataMahasiswa;
import id.ac.tazkia.smile.pddikti.service.PdDiktiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Locale;

@SpringBootTest
class SmilePddiktiApplicationTests {

	@Autowired private PdDiktiService service;
	@Autowired private ObjectMapper objectMapper;

	@Test
	void getToken() {
		String token = service.getToken();
		System.out.println("Token : "+token);
		Assertions.assertNotNull(token);
		Assertions.assertFalse(token.isEmpty());
	}


	@Test
	void insertGetIdMahasiswa() {
		BiodataMahasiswa biodata = new BiodataMahasiswa();
		biodata.setNama_mahasiswa("Test Api SMILE to PDdikti ");
		biodata.setJenis_kelamin("P");
		biodata.setTempat_lahir("BOGOR");
		LocalDate lc = LocalDate.parse("1995-01-01");
		biodata.setTanggal_lahir(lc);
		biodata.setId_agama("1");
		biodata.setTelepon("0857875454544");
		biodata.setHandphone("0857875454544");
		biodata.setEmail("haffizh@tazkia.ac.id");
		biodata.setNik("3201251312990005");
		biodata.setKewarganegaraan("ID");
		biodata.setKelurahan("CILEMBER");
		biodata.setPenerima_kps(0);
		biodata.setNama_ibu_kandung("Test Nama Ibu");
		biodata.setNama_ayah("Test Nama Ayah");
		biodata.setId_kebutuhan_khusus_mahasiswa(0);
		biodata.setId_kebutuhan_khusus_ayah(0);
		biodata.setId_kebutuhan_khusus_ibu(0);

		String idMahasiswa = service.insertMahasiswa(biodata);
//		String idMahasiswa = service.insertMahasiswa();
		System.out.println("idMahasiswa : "+idMahasiswa);
		Assertions.assertNotNull(idMahasiswa);
		Assertions.assertFalse(idMahasiswa.isEmpty());
	}
}
